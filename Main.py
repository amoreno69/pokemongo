import pymongo
import random
from Player import *
from Pokemon import *
from PokemonTeam import *
from Team import *
from Move import *
from mongoengine import *
from pymongo import MongoClient
from pprint import pprint
from bson.objectid import ObjectId

db = connect('projecte')

client = MongoClient()
#nos conectamos a db project
db = client["projecte"]
#nos conectamos a la collection pokemon que esta dentro de db (por tanto de project)
pokemons = db["pokemon"]
moves = db["move"]
teampokemons = db["pokemon_team"]
players = db["player"]
teams = db["team"]



def afegirPokemon(teamAConsultar, pokemonAAfegir):
    try:
        team = Team.objects(name=teamAConsultar)[0]
        pokemon = Pokemon.objects(name=pokemonAAfegir)[0]

        pokTeam = PokemonTeam(num=pokemon.num, name=pokemon.name, ptype=pokemon.ptype, weaknesses=pokemon.weaknesses, candy=pokemon.candy, candy_count=pokemon.candy_count)
        pokTeam.HPmax = random.randint(200,1000)
        pokTeam.HP = pokTeam.HPmax
        pokTeam.atk = random.randint(10,50)
        pokTeam.pdef = random.randint(10,50)
        pokTeam.CP = pokTeam.HPmax + pokTeam.atk + pokTeam.pdef

        movesAf = Moves()
        movesAf.charged = ChargedMove.objects[random.randint(0,ChargedMove.objects().count()-1)]
        movesAf.fast = FastMove.objects[random.randint(0,FastMove.objects().count()-1)]

        pokTeam.moves = movesAf

        team.pokemons.append(pokTeam)

        pokTeam.save()
        team.save()
        
    except:
        pprint("Error al fer la consulta")

while(True):
    print("PokeMONGO, Introdueix comanda, \"reiniciar\" per reiniciar la BBDD (\"bye\" per sortir)\n1.Query Nom/num atribut\n2.CreateTeam team\n3.Detach team Nom/num\n4.Add team nomPokemon\n5.AddUser NomTeam Alias\n6.Score Alias\n7.CreateUser name surname alias\n8.Combat\n------------------------------")
    lineaEntrada = input().split()

    if(lineaEntrada[0].lower()=='reiniciar'):
        teampokemons.drop()
        players.drop()
        teams.drop()
        us = Player(name="default", surname="default", alias="default")
        us.save()

    if(lineaEntrada[0].lower()=='bye'):
        break
    if(lineaEntrada[0].lower()=='query'):
        try:
            print('Modo busqueda')
            pokemonAConsultar = lineaEntrada[1]
            consulta = lineaEntrada[2]

            docnombres = ""

            if pokemonAConsultar.isdigit():
                docnombres = pokemons.find({"id":int(pokemonAConsultar)})
            else:
                docnombres = pokemons.find({"name":pokemonAConsultar})
                
            tuplaResultado = (docnombres[0]["name"],docnombres[0][consulta])
            pprint(tuplaResultado)
        except:
            pprint("Error al fer la consulta")

    if(lineaEntrada[0].lower()=='createteam'):
        try:
            nombreTeam = lineaEntrada[1]
            team = Team(name=nombreTeam, player=Player.objects(name="default")[0])
            team.save()
        except:
            pprint("Error al fer la consulta")

    if(lineaEntrada[0].lower()=='detach'):
        try:
            teamp = teams.find({"name":lineaEntrada[1]})
            pokemons = teamp[0]["pokemons"]
            tienes = False
            cont = 0
            for i in pokemons:cont+=1
            for i in pokemons:
                pokemon = teampokemons.find({"_id":ObjectId(i)})
                if(pokemon[0]["name"] == lineaEntrada[2] or pokemon[0]["num"] == lineaEntrada[2]):
                    teampokemons.delete_one({"_id":ObjectId(i)})
                    teams.update_one({},{"$pull":{"pokemons":ObjectId(i)}})
                    tienes = True
                    break

            if not tienes:
                print("No tens aquest Pokémon a l'equip",lineaEntrada[1])
            else:
                print( lineaEntrada[2], "alliberat de l’equip",lineaEntrada[1],"Nombre de Pokémon:",cont-1)
        except:
            pprint("Error al fer la consulta")
    
    if(lineaEntrada[0].lower()=='add'):
       afegirPokemon(lineaEntrada[1], lineaEntrada[2])
    
    if(lineaEntrada[0].lower()=='adduser'):
        try:
            playe = players.find_one({"alias":lineaEntrada[2]})
            teams.update_one({"name":lineaEntrada[1]},{"$set":{"player":playe["_id"]}})
            cont = 0
            for i in teams.find_one({"name":lineaEntrada[1]})["pokemons"]:
                cont+=1
            print(lineaEntrada[2], "associat a", lineaEntrada[1], "Nombre Pokémon",cont)
        except:
            pprint("Error al fer la consulta")

    if(lineaEntrada[0].lower()=="score"):
        try:
            players.update_one({"alias":lineaEntrada[1]}, {"$set":{"score":players.find_one({"alias":lineaEntrada[1]})["score"]+5}})
            print("Puntuació augmentada a",lineaEntrada[1], lineaEntrada[1],players.find_one({"alias":lineaEntrada[1]})["score"],"punts")
        except:
            pprint("Error al fer la consulta")

    if(lineaEntrada[0].lower()=='createuser'):
        try:
            player = Player(name=lineaEntrada[1],surname=lineaEntrada[2],alias=lineaEntrada[3])
            player.save()
        except:
            pprint("Error al fer la consulta")
    
    if(lineaEntrada[0].lower()=="combat"):
        #try:
            #Creació d'equips
            PlayerA = Player(name="PlayerA", surname="PlayerA", alias="PlayerA")
            PlayerB = Player(name="PlayerB", surname="PlayerB", alias="PlayerB")
            TeamA = Team(name="TeamA", player=PlayerA)
            TeamB = Team(name="TeamB", player=PlayerB)
            PlayerA.save()
            PlayerB.save()
            TeamA.save()
            TeamB.save()
            for i in range(6):
                afegirPokemon("TeamA", Pokemon.objects()[random.randint(0,150)].name)
                afegirPokemon("TeamB", Pokemon.objects()[random.randint(0,150)].name)
            TeamA = Team.objects(name="TeamA")[0]
            TeamB = Team.objects(name="TeamB")[0]
            PokemonActiuA = TeamA.pokemons[0]
            PokemonActiuB = TeamB.pokemons[0]
            acaba=False
            torn = 1
            while not acaba:
                #Jugador A ataca a Jugador B
                TeamA = Team.objects(name="TeamA")[0]
                TeamB = Team.objects(name="TeamB")[0]
                if torn == 1: 
                    acaba = True
                    for i in TeamA.pokemons:
                        if i.HP>0:acaba = False

                    if acaba:
                        break
                    print("Torn de",TeamA.name)
                    print("Pokemon A:\nNom:",PokemonActiuA.name,"\nHP:",PokemonActiuA.HP,"Energia",PokemonActiuA.energy)
                    print("Pokemon B:\nNom:",PokemonActiuB.name,"\nHP:",PokemonActiuB.HP,"Energia",PokemonActiuB.energy)
                    print("ATAC/canvi")
                    lineaEntrada = input().strip()
                    if lineaEntrada.lower() == "canvi":
                        eleg = False
                        while not eleg:
                            cont = 0
                            TeamA = Team.objects(name="TeamA")[0]
                            TeamB = Team.objects(name="TeamB")[0]
                            for i in TeamA.pokemons:
                                if i.HP > 0:
                                    print(cont,". ",i.name,sep="")
                                cont+=1
                            lineaEntrada = input().strip().split()
                            if(int(lineaEntrada[0])>= 0 and int(lineaEntrada[0])<=5):
                                if(TeamA.pokemons[int(lineaEntrada[0])].HP>0):
                                    eleg = True
                                    PokemonActiuA = TeamA.pokemons[int(lineaEntrada[0])]
                        PokemonActiuB.save()
                        PokemonActiuA.save()
                        TeamA = Team.objects(name="TeamA")[0]
                        TeamB = Team.objects(name="TeamB")[0]
                    else:
                        print("CHARGED/fast")
                        lineaEntrada = input().strip()
                        if lineaEntrada.lower() == "fast":
                            print("L'atac ha encertat")
                            dany = (5*PokemonActiuA.atk*PokemonActiuA.moves.fast.pwr)/(PokemonActiuB.pdef*2)
                            for i in PokemonActiuA.moves.fast.tipus:
                                if i in PokemonActiuB.weaknesses:
                                    dany*=2
                                    break
                                if i in PokemonActiuB.ptype:
                                    dany*=1.5

                            PokemonActiuB.HP-=dany
                            PokemonActiuA.energy+=PokemonActiuA.moves.fast.energyGain
                            if PokemonActiuB.HP <= 0:
                                print("Pokemon debilitat")
                                PokemonActiuB.save()
                                PokemonActiuA.save()
                                TeamA = Team.objects(name="TeamA")[0]
                                TeamB = Team.objects(name="TeamB")[0]
                                eleg = False
                                while not eleg:
                                    cont = 0
                                    for i in TeamB.pokemons:
                                        if i.HP > 0:
                                            print(cont,". ",i.name,sep="")
                                        cont+=1
                                    lineaEntrada = input().strip().split()
                                    if(int(lineaEntrada[0])>= 0 and int(lineaEntrada[0])<=5):
                                        if(TeamB.pokemons[int(lineaEntrada[0])].HP>0):
                                            eleg = True
                                            PokemonActiuB = TeamB.pokemons[int(lineaEntrada[0])] 
                        else:
                            if PokemonActiuA.energy<PokemonActiuA.moves.charged.energyCost:
                                print("L'atac ha fallat")
                            else:
                                print("L'atac ha encertat")
                                dany = (5*PokemonActiuA.atk*PokemonActiuA.moves.charged.pwr)/(PokemonActiuB.pdef*2)
                                for i in PokemonActiuA.moves.charged.tipus:
                                    if i in PokemonActiuB.weaknesses:
                                        dany*=2
                                        break
                                    if i in PokemonActiuB.ptype:
                                        dany*=1.5

                                PokemonActiuB.HP-=dany
                                if PokemonActiuB.HP <= 0:
                                    print("Pokemon debilitat")
                                    PokemonActiuB.save()
                                    PokemonActiuA.save()
                                    TeamA = Team.objects(name="TeamA")[0]
                                    TeamB = Team.objects(name="TeamB")[0]
                                    eleg = False
                                    while not eleg:
                                        cont = 0
                                        for i in TeamB.pokemons:
                                            if i.HP > 0:
                                                print(cont,". ",i.name,sep="")
                                            cont+=1
                                        lineaEntrada = input().strip().split()
                                        if(int(lineaEntrada[0])>= 0 and int(lineaEntrada[0])<=5):
                                            if(TeamB.pokemons[int(lineaEntrada[0])].HP>0):
                                                eleg = True
                                                PokemonActiuB = TeamB.pokemons[int(lineaEntrada[0])] 
                #Jugador B ataca a Jugador A
                else: 
                    acaba = True
                    for i in TeamB.pokemons:
                        if i.HP>0:acaba = False

                    if acaba:
                        break
                    print("Torn de",TeamB.name)
                    print("Pokemon A:\nNom:",PokemonActiuA.name,"\nHP:",PokemonActiuA.HP,"Energia\n",PokemonActiuA.energy)
                    print("Pokemon B:\nNom:",PokemonActiuB.name,"\nHP:",PokemonActiuB.HP,"Energia\n",PokemonActiuB.energy)
                    print("ATAC/canvi")
                    lineaEntrada = input().strip()
                    if lineaEntrada == "canvi":
                        eleg = False
                        while not eleg:
                            cont = 0
                            for i in TeamB.pokemons:
                                if i.HP > 0:
                                    print(cont,". ",i.name,sep="")
                                cont+=1
                            lineaEntrada = input().strip().split()
                            if(int(lineaEntrada[0])>= 0 and int(lineaEntrada[0])<=5):
                                if(TeamB.pokemons[int(lineaEntrada[0])].HP>0):
                                    eleg = True
                                    PokemonActiuB = TeamB.pokemons[int(lineaEntrada[0])]
                        PokemonActiuB.save()
                        PokemonActiuA.save()
                        TeamA = Team.objects(name="TeamA")[0]
                        TeamB = Team.objects(name="TeamB")[0]
                    else:
                        print("CHARGED/fast")
                        lineaEntrada = input().strip()
                        if lineaEntrada.lower() == "fast":
                            print("L'atac ha encertat")
                            dany = (5*PokemonActiuB.atk*PokemonActiuB.moves.fast.pwr)/(PokemonActiuA.pdef*2)
                            for i in PokemonActiuB.moves.fast.tipus:
                                if i in PokemonActiuA.weaknesses:
                                    dany*=2
                                    break
                                if i in PokemonActiuA.ptype:
                                    dany*=1.5

                            PokemonActiuA.HP-=dany
                            PokemonActiuB.energy+=PokemonActiuB.moves.fast.energyGain
                            if PokemonActiuB.energy > 110: PokemonActiuB.energy = 110
                            if PokemonActiuA.HP <= 0:
                                print("Pokemon debilitat")
                                PokemonActiuB.save()
                                PokemonActiuA.save()
                                TeamA = Team.objects(name="TeamA")[0]
                                TeamB = Team.objects(name="TeamB")[0]
                                eleg = False
                                while not eleg:
                                    cont = 0
                                    for i in TeamA.pokemons:
                                        if i.HP > 0:
                                            print(cont,". ",i.name,sep="")
                                        cont+=1
                                    lineaEntrada = input().strip().split()
                                    if(int(lineaEntrada[0])>= 0 and int(lineaEntrada[0])<=5):
                                        if(TeamA.pokemons[int(lineaEntrada[0])].HP>0):
                                            eleg = True
                                            PokemonActiuA = TeamA.pokemons[int(lineaEntrada[0])] 
                        else:
                            if PokemonActiuB.energy<PokemonActiuB.moves.charged.energyCost:
                                print("L'atac ha fallat")
                            else:
                                print("L'atac ha encertat")
                                dany = (5*PokemonActiuB.atk*PokemonActiuB.moves.charged.pwr)/(PokemonActiuA.pdef*2)
                                for i in PokemonActiuB.moves.charged.tipus:
                                    if i in PokemonActiuA.weaknesses:
                                        dany*=2
                                        break
                                    if i in PokemonActiuA.ptype:
                                        dany*=1.5

                                PokemonActiuA.HP-=dany
                                PokemonActiuB.energy-=PokemonActiuB.moves.charged.energyCost
                                if PokemonActiuA.HP <= 0:
                                    print("Pokemon debilitat")
                                    PokemonActiuB.save()
                                    PokemonActiuA.save()
                                    TeamA = Team.objects(name="TeamA")[0]
                                    TeamB = Team.objects(name="TeamB")[0]
                                    eleg = False
                                    while not eleg:
                                        cont = 0
                                        for i in TeamA.pokemons:
                                            if i.HP > 0:
                                                print(cont,". ",i.name,sep="")
                                            cont+=1
                                        lineaEntrada = input().strip().split()
                                        if(int(lineaEntrada[0])>= 0 and int(lineaEntrada[0])<=5):
                                            if(TeamA.pokemons[int(lineaEntrada[0])].HP>0):
                                                eleg = True
                                                PokemonActiuA = TeamA.pokemons[int(lineaEntrada[0])] 
                if torn == 1: torn = 2
                else: torn = 1
        #except:
            print("ERROR")