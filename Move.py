from mongoengine import *
from pprint import pprint

TYPES = ('Bug','Dark','Dragon','Electric','Fairy','Fight','Fire','Flying','Ghost','Grass','Ground','Ice','Normal','Poison','Psychic','Rock','Steel','Water')

class Move(Document):
    meta = {'allow_inheritance': True}
    name = StringField(required=True)
    pwr = FloatField(required=True, min_value=0, max_value=180)
    tipus = StringField(choices=TYPES, required=True)

class FastMove(Move):
    energyGain = IntField(required=True, min_value=0, max_value=35)

class ChargedMove(Move):
    energyCost = IntField(required=True, min_value=33, max_value=110)