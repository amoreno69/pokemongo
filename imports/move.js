db = db.getSiblingDB("project");
db.move.drop();
db.move.insertMany([{
    "_id": {
        "oid": "60828ec4aeb650daba842055"
    },
    "_cls": "Move.FastMove",
    "name": "Steel Wing",
    "pwr": 11,
    "tipus": "Steel",
    "energyGain": 7
},{
    "_id": {
        "oid": "60828ec4aeb650daba842056"
    },
    "_cls": "Move.FastMove",
    "name": "Dragon Tail",
    "pwr": 15,
    "tipus": "Dragon",
    "energyGain": 8
},{
    "_id": {
        "oid": "60828ec4aeb650daba842057"
    },
    "_cls": "Move.ChargedMove",
    "name": "Doom Desire",
    "pwr": 80,
    "tipus": "Steel",
    "energyCost": 50
},{
    "_id": {
        "oid": "60828ec4aeb650daba842058"
    },
    "_cls": "Move.ChargedMove",
    "name": "Future Sight",
    "pwr": 120,
    "tipus": "Psychic",
    "energyCost": 100
}])