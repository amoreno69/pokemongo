import pymongo
from pymongo import MongoClient
from mongoengine import *
from Player import *
from PokemonTeam import *
from pprint import pprint

class Team(Document):
    name = StringField(required = True)
    player = ReferenceField(Player, reverse_delete_rule=CASCADE)
    pokemons = ListField(ReferenceField(PokemonTeam, reverse_delete_rule=PULL))