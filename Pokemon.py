import pymongo
from pymongo import MongoClient
from mongoengine import *
import datetime
import random
from pprint import pprint

TYPES = ('Bug','Dark','Dragon','Electric','Fairy','Fight','Fire','Flying','Ghost','Grass','Ground','Ice','Normal','Poison','Psychic','Rock','Steel','Water')

#Tiene que estar obligatoriamente antes de Pokemon. Python carga por orden
class Evolution(EmbeddedDocument):
    num = StringField()
    name = StringField()

class Pokemon(Document):
    pid = FloatField(required = True, db_field='id')
    num = StringField(required = True)
    name = StringField(required = True)
    img = StringField(required = True)

    ptype = ListField(db_field='type', choice = TYPES, required = True)
    height = StringField(required = True)
    weight = StringField(required = True)
    candy = StringField(required = True)
    candy_count = FloatField(min_value = 0, max_value = 500)
    egg = StringField(required = True)
    spawn_chance = FloatField(required = True)
    avg_spawns = FloatField(required = True)
    spawn_time = StringField(required = True)
    multipliers = ListField(FloatField(),required = True)
    weaknesses = ListField(choice = TYPES,required = True)

    next_evolution = ListField(EmbeddedDocumentField('Evolution'))
    prev_evolution = ListField(EmbeddedDocumentField('Evolution'))

