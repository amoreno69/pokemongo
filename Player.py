import pymongo
from pymongo import MongoClient
from mongoengine import *
from pprint import pprint

class Player(Document):
    name = StringField(required = True)
    surname = StringField(required = True)
    alias = StringField(required = True)
    score = FloatField(default = 0)