import pymongo
import datetime
from pymongo import MongoClient
from mongoengine import *
from Move import *
from pprint import pprint

TYPES = ('Bug','Dark','Dragon','Electric','Fairy','Fight','Fire','Flying','Ghost','Grass','Ground','Ice','Normal','Poison','Psychic','Rock','Steel','Water')


class Moves(EmbeddedDocument):
    fast = ReferenceField(FastMove)
    charged = ReferenceField(ChargedMove)

class PokemonTeam(Document):
    num = StringField(required = True)
    name = StringField(required = True)
    ptype = ListField(db_field='type', choice = TYPES, required = True)
    catch_date = DateTimeField(default=datetime.datetime.now)

    CP = FloatField(required = True)
    HPmax = FloatField(required = True, min_value = 200, max_value = 1000)
    HP = FloatField(default = HPmax, min_value = 200, max_value = 1000)
    atk = FloatField(required = True, min_value = 10, max_value = 50)
    pdef = FloatField(required = True, db_field='def', min_value = 10, max_value = 50)
    energy = FloatField(required = True, min_value = 0, max_value = 500, default = 0)
    
    moves = EmbeddedDocumentField("Moves", required = True)

    candy = StringField(required = True)
    candy_count = FloatField(min_value = 0, max_value = 500)
    current_candy = FloatField(min_value = 0, max_value = 500, default = 0)
    weaknesses = ListField(choice = TYPES, required = True)